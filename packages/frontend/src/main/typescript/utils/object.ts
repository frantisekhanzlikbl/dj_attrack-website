export const objectUtils = {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	filter: <ObjectType>(object: ObjectType, keys: (keyof ObjectType)[], blacklist = false) => {
		return Object.fromEntries(Object.entries(object).filter(it => keys.includes(it[0] as keyof ObjectType) !== blacklist))
	},
}
