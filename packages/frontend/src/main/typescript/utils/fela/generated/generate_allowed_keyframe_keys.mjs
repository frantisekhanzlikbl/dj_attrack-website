import { inspect } from "util"
import { promises as Fs } from "fs"
;(async () => {
	const pad = (number, width, paddingCharacter = "0") => {
		number = number + ""
		return number.length >= width ? number : new Array(width - number.length + 1).join(paddingCharacter) + number
	}

	const percentageKeys = Array.from({ length: 101 }, (_, index) => `${pad(index, 3)}%`)
	const additionalKeys = ["from", "to"]

	const keys = percentageKeys.concat(additionalKeys)

	console.log(`using keys: ${inspect(keys, { maxArrayLength: null, colors: true })}`)
	await Fs.writeFile(
		"allowed_keyframe_keys.ts",
		`/* eslint-disable prettier/prettier */

/*********************************/
/* GENERATED FILE; DON'T MODIFY! */
/*********************************/

export type allowedKeyframeKeys = ${keys.map(it => `"${it}"`).join(" | ")}
`,
	)
})()
