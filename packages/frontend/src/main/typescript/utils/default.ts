export const def = <ValueType>(value: ValueType, defaultValue: NonNullable<ValueType>) => (typeof value == "undefined" || value == null ? defaultValue : value) as NonNullable<ValueType>
