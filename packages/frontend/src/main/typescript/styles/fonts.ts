import { FelaStyleObject } from "../utils/fela/types"

const fontNames = {
	serif: "serif",
	robotoSlab: "Roboto Slab",
	staatliches: "Staatliches",
	montserrat: "Montserrat",
}

const specialFontProperties: Partial<Record<keyof typeof fontNames, FelaStyleObject>> = {
	staatliches: {
		letterSpacing: "0.1em",
	},
}

const fallbackFontName = fontNames.serif

const getCssFontDefinition = (fontName: string) => `"${fontName}", "${fallbackFontName}"`

export const fonts = Object.fromEntries(
	(Object.entries(fontNames) as [keyof typeof fontNames, string][]).map(it => [
		it[0],
		{
			fontFamily: getCssFontDefinition(it[1]),
			...specialFontProperties[it[0]],
		},
	]),
) as Record<keyof typeof fontNames, FelaStyleObject>
