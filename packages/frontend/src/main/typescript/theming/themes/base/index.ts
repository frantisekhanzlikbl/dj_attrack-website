import { Theme } from "../../theme"

/* Components */

import { serviceList } from "./components/service_list"
import { layout } from "./components/layout"
import { shadows } from "./common/shadows"
import { bigButtonBar } from "./components/big_button_list"
import { navbar } from "./components/navbar"

/* Theme definition */

export const baseTheme: Theme = {
	common: {
		shadows: shadows(() => baseTheme),
	},
	components: {
		layout: layout(() => baseTheme),
		navbar: navbar(() => baseTheme),
		bigButton: bigButtonBar(() => baseTheme).subcomponents.bigButton,
		serviceList: serviceList(() => baseTheme),
	},
}
