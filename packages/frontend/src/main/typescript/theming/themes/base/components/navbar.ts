import { Theme } from "../../../theme"
import { BoundingBox } from "../../../properties/bounding_box"

let themeDummy: Theme

export const navbar: (getTheme: () => Theme) => typeof themeDummy.components.navbar = () => ({
	section: {
		bounds: [BoundingBox.newXY({ x: "1rem" })],
		link: {
			text: {
				color: "white",
			},
		},
	},
})
