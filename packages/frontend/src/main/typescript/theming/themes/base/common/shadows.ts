import { Shadow } from "../../../properties/shadow"
import { Point } from "../../../properties/point"
import { Theme } from "../../../theme"

let themeDummy: Theme

export const shadows: (getTheme: () => Theme) => typeof themeDummy.common.shadows = () => ({
	dark: {
		normal: Object.assign(new Shadow(), {
			offset: Object.assign(new Point(), {
				x: "0.5rem",
				y: "0.5rem",
			}),
			blur: "0.5rem",
			color: "hsla(0, 0%, 0%, 0.2)",
		}),
		inset: Object.assign(new Shadow(), {
			isInset: true,
			offset: Object.assign(new Point(), {
				x: "0",
				y: "0",
			}),
			size: "0",
			blur: "1rem",
			color: "hsla(0, 0%, 0%, 0.5)",
		}),
	},
	glow: {
		normal: Object.assign(new Shadow(), {
			offset: Object.assign(new Point(), {
				x: "0",
				y: "0.2rem",
			}),
			size: "0rem",
			blur: "3rem",
			color: "hsl(123deg, 100%, 25%)",
		}),
	},
})
