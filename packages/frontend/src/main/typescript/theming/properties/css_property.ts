import { FelaStyleObject } from "../../utils/fela/types"

export interface CssProperty {
	toFelaStyle(): FelaStyleObject
}
