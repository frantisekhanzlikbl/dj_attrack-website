import React, { useEffect, useState } from "react"
import { FelaComponent } from "react-fela"
import { Theme } from "../../theming/theme"
import { ImageModel } from "../../models/image"
import { Image } from "../reusable/image"
import { flatMapObjectArray } from "../../utils/array"
import { FelaStyle } from "../../utils/fela/types"
import { BoundingBox } from "../../theming/properties/bounding_box"
import { def } from "../../utils/default"
import _ from "lodash"

export interface ThemeStyles {
	section: {
		bounds: BoundingBox[]
		link: {
			text: {
				color: string
			}
		}
	}
}

interface Section {
	id: string
	name: string
	url: string
	icon?: ImageModel
}

interface NavbarProps {
	sections: Section[]
}

export const styles = {
	navbar: (props => ({
		width: "100%",
		height: "100%",
		display: "flex",
		justifyContent: "center",
		paddingBottom: "0.5rem",
		backgroundColor: "hsla(0, 0%, 0%, 0)",
		borderBottom: "2px solid",
		borderBottomColor: "hsla(0, 0%, 0%, 0)",
		transition: "background-color 1s 0s, border-bottom-color 1s 0s",
		...(def(props.isPageScrolled, false)
			? {
					backgroundColor: "hsla(0, 0%, 0%, 1)",
					borderBottomColor: "2px solid white",
					transition: "background-color 1s 0s, border-bottom-color 1s 0.5s",
			  }
			: {}),
	})) as FelaStyle<Theme, { isPageScrolled: boolean }>,
	section: (props => ({
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		height: "100%",
		paddingLeft: "1rem",
		paddingRight: "1rem",
		...flatMapObjectArray(props.theme.components.navbar.section.bounds, it => it.toFelaStyle()),
		fontSize: "1.25em",
		color: props.theme.components.navbar.section.link.text.color,
		textDecoration: "none",
		borderBottomLeftRadius: "0.25rem",
		borderBottomRightRadius: "0.25rem",
		backgroundColor: "hsla(0, 0%, 100%, 0)",
		transition: "background-color 0.5s",
		"> .navbar\\/section\\/label": {
			transition: "transform 0.5s ease",
		},
		"> .navbar\\/section\\/icon": {
			position: "absolute",
			transition: "transform 0.5s ease, opacity 0.5s",
			transform: "translate(0, -50%)",
			opacity: "0",
		},
		":hover": {
			color: props.theme.components.navbar.section.link.text.color,
			textDecoration: "none",
			backgroundColor: "hsla(0, 0%, 25%, 1)",
			"> .navbar\\/section\\/label": {
				transform: "translate(0, 40%)",
			},
			"> .navbar\\/section\\/icon": {
				transform: "translate(0, -35%)",
				opacity: "1",
			},
		},
	})) as FelaStyle<Theme>,
}

export const Navbar: React.FunctionComponent<NavbarProps> = properties => {
	const [isPageScrolled, setIsPageScrolled] = useState(false)
	useEffect(() => {
		document.addEventListener(
			"scroll",
			_.throttle(() => setIsPageScrolled(window.scrollY !== 0), 100),
			{ passive: true },
		)
	})

	return (
		<FelaComponent style={styles.navbar} as="nav" isPageScrolled={isPageScrolled}>
			{properties.sections.map(section => (
				<FelaComponent key={section.id} style={styles.section}>
					{({ className }) => (
						<a key={section.id} href={section.url} className={className}>
							{section.icon == null ? null : <Image className="navbar/section/icon" image={section.icon} />}
							<span className="navbar/section/label">{section.name}</span>
						</a>
					)}
				</FelaComponent>
			))}
		</FelaComponent>
	)
}
