import React from "react"
import Head from "next/head"
import { Layout } from "../layout/layout"
import { Slider } from "../reusable/slider"
// import { BigButtonBar } from "../reusable/big_button_bar/BigButtonBar"
// import { BigButton } from "../reusable/big_button_bar/BigButton"
import { Theme } from "../../theming/theme"
import { Spacer, Orientation } from "../reusable/spacer"
import { ContactList } from "../reusable/contact_list"
import { DataProviderContext } from "../../react_contexts/data_provider"
import { NumberRange } from "../../utils/range"
import { ServiceList } from "../reusable/service_list"
import { FelaComponent } from "react-fela"
import { FelaStyle, allowUnsafeFelaStyleKeys } from "../../utils/fela/types"
import { fonts } from "../../styles/fonts"
import { Image } from "../reusable/image"

export const styles: Record<"slider" | "sliderOverlay" | "sliderOverlayImage" | "sectionLabel" | "fullwidthDividerContainer" | "fullwidthDivider", FelaStyle<Theme>> = {
	slider: props => ({
		marginTop: `calc(-${props.theme.components.layout.composition.navbar.height})`,
		width: "100%",
		height: `calc(100vh)`,
	}),
	sliderOverlay: allowUnsafeFelaStyleKeys({
		position: "absolute",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "flex-end",
		paddingTop: "calc(5rem + 10vh)",
		paddingBottom: "2rem",
		width: "100%",
		height: "100vh",
		...fonts.staatliches,
		fontWeight: "bold",
		fontSize: "2em",
		"@media (min-width: 1200px)": {
			fontSize: "10em",
		},
	}),
	sliderOverlayImage: allowUnsafeFelaStyleKeys({
		position: "absolute",
		top: "50%",
		display: "block",
		filter: "drop-shadow(0 0 1rem white)",
		width: "10rem",
		height: "10rem",
		"@media (min-width: 1200px)": {
			width: "20rem",
			height: "20rem",
		},
	}),
	sectionLabel: {
		fontSize: "2em",
		marginBottom: "0.5em",
		paddingRight: "2em",
		display: "inline-block",
		borderBottom: "0.05em solid currentColor",
	},
	fullwidthDividerContainer: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		width: "100%",
	},
	fullwidthDivider: {
		fontSize: "2em",
		textShadow: "5px 2.5px 5px rgba(255, 255, 255, 0.5)",
		fontWeight: "bold",
	},
}
export class Home extends React.Component {
	public render() {
		// const bigButtons = [
		// 	{
		// 		label: "Galerie",
		// 		targetUrl: "/gallery",
		// 		iconUrl: "/static/images/buttons/icons/gallery.svg",
		// 	},
		// 	{
		// 		label: "Videa",
		// 		targetUrl: "#videos",
		// 		iconUrl: "/static/images/buttons/icons/videos.svg",
		// 	},
		// 	{
		// 		label: "Kontakty",
		// 		targetUrl: "#contacts",
		// 		iconUrl: "/static/images/buttons/icons/contacts.svg",
		// 	},
		// ]
		return (
			<DataProviderContext.Consumer>
				{dataProvider => (
					<>
						<Head>
							<script src="https://kit.fontawesome.com/a907a08ce3.js" integrity="sha384-7cupKsyf1krt0hPaFhz7o347RIzmECgbuiiOTe+NbIi9eA5z8RSNuYFdTDgRTZHH" crossOrigin="anonymous" />
						</Head>
						<Layout>
							<FelaComponent style={styles.slider}>
								{({ className }) => (
									<FelaComponent style={styles.fullwidthDividerContainer}>
										<Slider className={className}>
											{new NumberRange(1, 5).map(index => (
												<img key={index} src={`/static/images/slider/slides/0${index}.jpeg`} alt={`Slider image #${index}`} />
											))}
										</Slider>
									</FelaComponent>
								)}
							</FelaComponent>
							<FelaComponent style={styles.sliderOverlay}>
								<FelaComponent style={styles.sliderOverlayImage}>
									{({ className }) => (
										<Image
											className={className}
											image={{
												url: "/static/images/logos/0.2.png",
												altText: "",
											}}
										/>
									)}
								</FelaComponent>
								DJ Attract
							</FelaComponent>
							{/* <BigButtonBar>
									{bigButtons.map(button => (
										<BigButton key={button.label} targetUrl={button.targetUrl} iconUrl={button.iconUrl}>
											{button.label}
										</BigButton>
									))}
								</BigButtonBar> */}
							<DataProviderContext.Consumer>{dataProvider => <ServiceList services={dataProvider.getServices()} />}</DataProviderContext.Consumer>
							<Spacer width="10rem" />
							<FelaComponent style={styles.fullwidthDividerContainer}>
								<FelaComponent style={styles.fullwidthDivider}>
									<Spacer orientation={Orientation.HORIZONTAL} width="0.25em" />
									Kontakt
								</FelaComponent>
							</FelaComponent>
							<FelaComponent style={styles.sectionLabel}>Kontaktní osoby:</FelaComponent>
							<ContactList people={dataProvider.getContacts()} />
							<Spacer width="10rem" />
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Soluta nisi sed, ea, aliquam numquam quisquam provident praesentium tempore accusantium laborum ad. Aspernatur eum
							saepe reprehenderit dolorum deserunt nulla laudantium reiciendis.
						</Layout>
					</>
				)}
			</DataProviderContext.Consumer>
		)
	}
}
