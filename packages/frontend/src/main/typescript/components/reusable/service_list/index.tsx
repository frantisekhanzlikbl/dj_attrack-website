import React from "react"
import { FelaComponent } from "react-fela"
import { ServiceModel } from "../../../models/service"
import { ServiceButton } from "./components/service_button_prototype"
import { styles } from "./styles"

interface ServiceListProps {
	services: ServiceModel[]
}

export class ServiceList extends React.Component<ServiceListProps> {
	public render() {
		return (
			<FelaComponent style={styles.serviceList}>
				{this.props.services.map(service => (
					<ServiceButton key={service.id} service={service} targetUrl={`/service/${service}`} /> // TODO: Export service url construction to some helper function
				))}
			</FelaComponent>
		)
	}
}
