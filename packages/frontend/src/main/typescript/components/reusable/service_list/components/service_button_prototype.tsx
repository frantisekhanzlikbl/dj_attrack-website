import React from "react"
import Link from "next/link"
import { Image } from "../../image"
import { FelaComponent } from "react-fela"
import { Shadow } from "../../../../theming/properties/shadow"
import { fonts } from "../../../../styles/fonts"
import { FelaStyleObject, allowUnsafeFelaStyleKeysOnRecord } from "../../../../utils/fela/types"
import { normalization } from "../../../../styles/normalization"
import { effects } from "../../../../styles/effects"
import { objectUtils } from "../../../../utils/object"
import { ServiceModel } from "../../../../models/service"

export interface ThemeStyles {}

export interface ServiceButtonProps {
	service: ServiceModel
	targetUrl: string
}

const styles: Record<"card" | "cardTop" | "imageWrapper" | "image" | "button" | "label", FelaStyleObject> = allowUnsafeFelaStyleKeysOnRecord({
	card: {
		position: "relative",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "space-between",
		width: "15rem",
		height: "22.5rem",
		padding: "2rem",
		border: "1px solid gold",
		borderRadius: "5px",
		// backgroundColor: "hsla(0, 0%, 0%, 0.5)",
		backgroundSize: "100% 200%",
		backgroundImage: `linear-gradient(to bottom, hsl(40deg, 65%, 0%) 0%, hsl(40deg, 65%, 0%) ${(100 * 100) / 200}%, hsl(40deg, 65%, 7%) 100%)`,
		backgroundPositionY: "0",
		transition: "background-position-y 0.5s",
		...normalization.link,
		...Object.assign(new Shadow(), {
			isInset: true,
			size: "0.5rem",
			color: "hsla(30deg, 100%, 20%)",
			blur: "0.5rem",
		}).toFelaStyle(),
		"::before": {
			content: '" "',
			display: "block",
			position: "absolute",
			left: 0,
			right: 0,
			bottom: 0,
			top: 0,
			...Object.assign(new Shadow(), {
				isInset: true,
				color: "hsla(46deg, 65%, 30%)",
				blur: "0.5rem",
			}).toFelaStyle(),
		},
		":hover": {
			// backgroundColor: "hsl(46deg, 65%, 5%)",
			backgroundPositionY: "100%",
			"> .card_top .card_image_wrapper": {
				top: "-5%",
				filter: "grayscale(0) saturate(1.25)",
				...effects.shine[":hover"],
			},
			"> .card_button": {
				backgroundColor: "gold",
				color: "black",
			},
			...(normalization.link[":hover"] as FelaStyleObject),
		},
	},
	cardTop: {
		position: "relative",
		display: "flex",
		flexDirection: "column",
		alignItems: "space-between",
		width: "100%",
		marginTop: "-5rem",
	},
	imageWrapper: {
		filter: "grayscale(0.3) saturate(0.8)",
		top: "0",
		transition: "top 0.5s ease-out, filter 0.5s ease-out",
		...objectUtils.filter(effects.shine, [":hover"], true),
	},
	image: {
		width: "100%",
		height: "100%",
		objectFit: "cover",
	},
	label: {
		fontSize: "1.5em",
		textAlign: "center",
		...fonts.robotoSlab,
		...normalization.link,
	},
	button: {
		...normalization.button,
		width: "100%",
		height: "3rem",
		padding: "0",
		border: "2px solid gold",
		borderRadius: "1.5rem",
		color: "gold",
		...fonts.staatliches,
		fontSize: "1.25em",
		transition: "color 0.5s, background-color 0.5s",
	},
})

export class ServiceButton extends React.Component<ServiceButtonProps> {
	public render() {
		return (
			<FelaComponent style={styles.card}>
				{({ className }) => (
					<Link href={this.props.targetUrl}>
						<a className={className}>
							<FelaComponent style={styles.cardTop}>
								{({ className }) => (
									<div className={className + " card_top"}>
										<FelaComponent style={styles.imageWrapper}>
											{({ className }) => (
												<div className={className + " card_image_wrapper"}>
													<FelaComponent style={styles.image}>{({ className }) => <Image className={className} image={this.props.service.image} />}</FelaComponent>
												</div>
											)}
										</FelaComponent>
										<FelaComponent style={styles.label}>{this.props.service.name}</FelaComponent>
									</div>
								)}
							</FelaComponent>
							<FelaComponent style={styles.button}>{({ className }) => <button className={className + " card_button"}>zobrazit</button>}</FelaComponent>
						</a>
					</Link>
				)}
			</FelaComponent>
		)
	}
}
