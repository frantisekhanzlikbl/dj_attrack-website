import React from "react"
import SlickSlider from "react-slick"
import { def } from "../../utils/default"
import Head from "next/head"
import { FelaComponent } from "react-fela"

import "../../../stylus/components/slider/slick.styl"

interface SliderProps {
	children?: React.ReactNode
	className?: string
	infinite?: boolean
	autoplay?: boolean
}

interface SliderArrowDirection {
	fileName: string
	className: string
}

const commonSliderArrowDirections: { [key: string]: SliderArrowDirection } = {
	PREVIOUS: { fileName: "previous", className: "arrow-previous" },
	NEXT: { fileName: "next", className: "arrow-next" },
}

interface SliderArrowProps {
	direction: SliderArrowDirection
	// Slick props
	slideCount?: number
	currentSlider?: number
	onClick?: (event: React.MouseEvent<HTMLImageElement, MouseEvent>) => void
}

class SliderArrow extends React.Component<SliderArrowProps> {
	public render() {
		return (
			<FelaComponent
				style={{
					position: "absolute",
					top: "50%",
					zIndex: 1,
					width: "64px",
					height: "64px",
					transform: "translate(0,-50%)",
					cursor: "pointer",
				}}
			>
				{({ className }) => (
					<img className={`${className} ${this.props.direction.className}`} src={`/static/images/slider/arrows/${this.props.direction.fileName}.svg`} onClick={this.props.onClick} />
				)}
			</FelaComponent>
		)
	}
}

export class Slider extends React.Component<SliderProps> {
	public render() {
		return (
			<>
				<Head>
					<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
					<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
				</Head>
				<SlickSlider
					{...{
						infinite: def(this.props.infinite, true),
						autoplay: def(this.props.autoplay, true),
						prevArrow: <SliderArrow direction={commonSliderArrowDirections.PREVIOUS} />,
						nextArrow: <SliderArrow direction={commonSliderArrowDirections.NEXT} />,
						className: def(this.props.className, ""),
					}}
				>
					{this.props.children}
				</SlickSlider>
			</>
		)
	}
}
