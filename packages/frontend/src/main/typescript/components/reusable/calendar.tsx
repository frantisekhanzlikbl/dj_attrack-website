import React from "react"

export class Calendar extends React.Component {
	public render() {
		return (
			<iframe
				src="https://outlook.live.com/owa/calendar/00000000-0000-0000-0000-000000000000/1d6a2f9e-af1f-4a50-9ff6-8989fae6af50/cid-2E2F12D855F6093E/index.html"
				style={{
					border: "2px solid rgb(150, 0, 255)",
					borderRadius: "0.5rem",
				}}
				frameBorder={0}
				width={1000}
				height={700}
			/>
		)
	}
}
