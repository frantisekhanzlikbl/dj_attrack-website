import { ImageModel } from "./image"

export interface ServiceModel {
	id: string
	name: string
	description: string
	image: ImageModel
}
