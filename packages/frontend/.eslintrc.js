module.exports = {
	root: true,
	parser: "@typescript-eslint/parser", // Specifies the ESLint parser
	plugins: ["prettier", "filenames"],
	extends: [
		"plugin:react/recommended", // Uses the recommended rules from @eslint-plugin-react
		"plugin:@typescript-eslint/recommended", // Uses the recommended rules from @typescript-eslint/eslint-plugin
		"prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
		"plugin:prettier/recommended",
	],
	parserOptions: {
		ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
		sourceType: "module", // Allows for the use of imports
		ecmaFeatures: {
			jsx: true, // Allows for the parsing of JSX
		},
	},
	rules: {
		"prettier/prettier": "error",
		"react/self-closing-comp": ["error"],
		"@typescript-eslint/explicit-function-return-type": ["off"],
		"@typescript-eslint/no-parameter-properties": ["off"],
		"@typescript-eslint/no-empty-interface": ["off"],
		"filenames/match-regex": [2, "^[a-z._]*((\\[([a-z][a-z_]*[a-z]|[a-z])\\][a-z_]+)*\\[([a-z][a-z_]*[a-z]|[a-z])\\](\\.[a-z]+)*)?$", true],
	},
	settings: {
		react: {
			version: "detect", // Tells eslint-plugin-react to automatically detect the version of React to use
		},
	},
}
